/* Ini adalah file main.js yang akan menjadi tempat menulis javascript */

//=============================================================================================================================//
//=============================== Deklarasi Array Penampung Soal-Soal Quiz dan Kunci Jawaban ==================================//
//=============================================================================================================================//

const Data = [
  {
    question: "Siapa Pencipta Lagu Indonesia Raya",
    a: " Ibu Soed ",
    b: " Ismail Marzuki ",
    c: " Semua Salah ",
    correct: "c",
  },
  {
    question: "Siapakah Presiden RI yang ke-2?",
    a: " B.J. Habibie ",
    b: " Soeharto ",
    c: " Soekarno ",
    correct: "b",
  },
  {
    question: "Planet ke-7 dari Sistem Tata Surya Kita, adalah ?",
    a: "Merkurius",
    b: "Saturnus",
    c: "Semua Salah",
    correct: "c",
  },
  {
    question: " Peristiwa Benda Padat Berubah Menjadi Gas disebut? ",
    a: " Mencair ",
    b: " Menyublim ",
    c: " Membeku ",
    correct: "b",
  },
  {
    question: " Peristiwa Benda Padat Berubah Menjadi Cair disebut? ",
    a: " Mencair ",
    b: " Menyublim ",
    c: " Membeku ",
    correct: "a",
  },
  {
    question: "Siapa Penemu Bohlam Lampu?",
    a: " Nicholas Tesla ",
    b: " Charles GoodYear ",
    c: " Semua Salah ",
    correct: "c",
  },
  {
    question: "Pada Perang Dunia II, Jepang di bom di Kota?",
    a: " Tokyo ",
    b: " Mishima ",
    c: " Semua Salah ",
    correct: "c",
  },
  {
    question: "Yang Bukan Merk Mobil jepang, adalah ?",
    a: "Honda",
    b: "Suzuki",
    c: "Kawasaki",
    correct: "c",
  },
  {
    question: " Apa Kepanjangan dari Covid-19 ",
    a: " code virus desease 19 ",
    b: " corona virus desease code 19 ",
    c: " coordinate virus desease no. 19 ",
    correct: "b",
  },
  {
    question: " Memory pada Sebuah Komputer, adalah? ",
    a: " Hardisk ",
    b: " MotherBoard ",
    c: " RAM ",
    correct: "c",
  },
];

//=============================================================================================================================//
//=============================== Deklarasi Array Penampung Soal-Soal Quiz dan Kunci Jawaban ==================================//
//=============================================================================================================================//




//=============================================================================================================================//
//======================================= Deklarasi Variabel - Variabel Functions =============================================//
//=============================================================================================================================//


//=====================================================================================//
//================= Deklarasi Variabel Penampung Opsi Pilihan Ganda ==================//
//===================================================================================//
const result = document.getElementById("result");
const option1 = document.getElementById("option1");
const option2 = document.getElementById("option2");
const option3 = document.getElementById("option3");
const question = document.querySelector("h3");
//=====================================================================================//
//================= Deklarasi Variabel Penampung Opsi Pilihan Ganda ==================//
//===================================================================================//


//=====================================================================================//
//============ Deklarasi Variabel Penampung Pilihan Ganda Yang Diseleksi =============//
//===================================================================================//
const inputA = document.getElementById("a");
const inputB = document.getElementById("b");
const inputC = document.getElementById("c");

let states = []
//=====================================================================================//
//============ Deklarasi Variabel Penampung Pilihan Ganda Yang Diseleksi =============//
//===================================================================================//


//=====================================================================================//
//=========== Deklarasi Variabel Agar Form Nama dan Quiz Bisa Show / Hide ============//
//===================================================================================//
let questionWrap = document.getElementById("quiz");
let startbtn = document.getElementById("start");
let formWrap = document.getElementById("form");
//=====================================================================================//
//=========== Deklarasi Variabel Agar Form Nama dan Quiz Bisa Show / Hide ============//
//===================================================================================//


//=====================================================================================//
//=================== Deklarasi Variabel Button Next Dan Previous ====================//
//===================================================================================//
let nextBtn = document.getElementById("btn-next");
let prevBtn = document.getElementById("btn-prev");
//=====================================================================================//
//=================== Deklarasi Variabel Button Next Dan Previous ====================//
//===================================================================================//


//=====================================================================================//
//============ Deklarasi Variabel Body Quiz dan Jawaban dan Initial Value ============//
//===================================================================================//
const quiz = document.getElementById("quiz");
const answers = document.querySelectorAll(".answer");

let initialQuize = 0;
let score = 0;
//=====================================================================================//
//============ Deklarasi Variabel Body Quiz dan Jawaban dan Initial Value ============//
//===================================================================================//


//=====================================================================================//
//======================= Deklarasi Variabel untuk File Audio ========================//
//===================================================================================//
let audio = new Audio('Remix.mp3');
let applause = new Audio('applause.mp3');
let dissapointed = new Audio('Disappointed.mp3');

//=====================================================================================//
//======================= Deklarasi Variabel untuk File Audio ========================//
//===================================================================================//


//=====================================================================================//
//====== Deklarasi Variabel Menyimpan Nama User dan Alert Minimum Karakter Nama ======//
//===================================================================================//
const studentName = document.getElementById("studentName");
const alert = document.getElementById("promptAlert");
//=====================================================================================//
//====== Deklarasi Variabel Menyimpan Nama User dan Alert Minimum Karakter Nama ======//
//===================================================================================//


//=====================================================================================//
//=================== Deklarasi Variabel Untuk Fungsi DarkMode =======================//
//===================================================================================//
let darkMode = false
const darkModeBtn = document.getElementById('Mode-btn');
const container = document.getElementById('home');
//=====================================================================================//
//=================== Deklarasi Variabel Untuk Fungsi DarkMode =======================//
//===================================================================================//


//=============================================================================================================================//
//======================================= Deklarasi Variabel - Variabel Functions =============================================//
//=============================================================================================================================//





//=============================================================================================================================//
//=========================================== FUNCTIONS - FUNCTIONS YANG DIGUNAKAN ============================================//
//=============================================================================================================================//



//====================================================================================//
//=========== Function untuk Menyimpan Seleksi Opsi Jawaban yang Di Input ===========//
//==================================================================================//
function collectUserAnswer() {
  allQuestions[pos].userAnswer =
      document.querySelector('input[name="choice"]:checked').value; 
}

function createChoices() {
  for(let i = 0; i < 4; i++) {
      let choice = document.getElementsByTagName("p")[i];
      let option = "<input id='ans_" + i 
          + "' type='radio' name='choice' value='" 
          + i + "'";

      if ( i == allQuestions[pos].userAnswer ) {
          option += " checked='checked'";   
      }
      option +=  ">" + allQuestions[pos].choices[i] 
             + "</input>";

      choice.innerHTML = option;
  }
}

function getValue() {
  let value = undefined;
  answers.forEach((answer) => {
    if (answer.checked) {
      value = answer.id;
      states[initialQuize] = {answer: answer.id, correct: answer.id === Data[initialQuize].correct ? true : false}
      localStorage.setItem('saveStates', JSON.stringify(states))
      console.log(states.filter(item => item.correct === true).length);
    }
  });
  return value;
}

function unCheckAnswer() {
  answers.forEach((answer) => {
    answer.checked = false;
  });
}
//====================================================================================//
//=========== Function untuk Menyimpan Seleksi Opsi Jawaban yang Di Input ===========//
//==================================================================================//


//===================================================================================//
//======= Function untuk Show / Hide Body Form Nama dan Quiz saat Klik Start =======//
//=================================================================================//
startbtn.addEventListener("click", function(){
  startQuiz()
});

function startQuiz(){
  if (studentName.value.length < 4){
    alert.style.display = 'block'
  } else {
  formWrap.style.display = 'none';
  questionWrap.style.display = 'block';
  audio.play();
  audio.loop = true;
  localStorage.setItem('name', studentName.value);
  loadQuize();
  }
}
//===================================================================================//
//======= Function untuk Show / Hide Body Form Nama dan Quiz saat Klik Start =======//
//=================================================================================//


//=====================================================================================//
//==== Function untuk Mulai Menjalankan / Load Quiz, Dipakai di Next/Prev Button =====//
//===================================================================================//
loadQuize();
function loadQuize() {
  // unCheckAnswer();
  switch(states[initialQuize]?.answer){
    case "a":
      inputA.checked = true;
      break;
    case "b":
      inputB.checked = true;
      break;
    case "c":
      inputC.checked = true;
      break;
    default:
      unCheckAnswer()
  }
  let nextOption = Data[initialQuize];
  question.innerText = nextOption.question;
  option1.innerText = nextOption.a;
  option2.innerText = nextOption.b;
  option3.innerText = nextOption.c;
  nextBtn.addEventListener("click", nextQuestion);
  prevBtn.addEventListener("click", PreviousQuestion);
}
//=====================================================================================//
//==== Function untuk Mulai Menjalankan / Load Quiz, Dipakai di Next/Prev Button =====//
//===================================================================================//



//=====================================================================================//
//============================= Function CheckStorage  ===============================//
//===================================================================================//
function checkStorage (){
  if (localStorage.getItem('name') != null ){
    formWrap.style.display = 'none';
    questionWrap.style.display = 'block';
  }
  if (localStorage.getItem('indexQuiz') === null && localStorage.getItem('saveStates') === null && localStorage.getItem('darkMode') === null) {
    localStorage.setItem('indexQuiz', 0) 
    localStorage.setItem('saveStates', JSON.stringify([]))
    localStorage.setItem('darkMode', false)
  } else {
    initialQuize = localStorage.getItem('indexQuiz')
    states = JSON.parse(localStorage.getItem('saveStates'));
    darkMode = localStorage.getItem('darkMode');
  }
}

checkStorage ()
//=====================================================================================//
//============================= Function CheckStorage  ===============================//
//===================================================================================//

const repBtn = document.getElementById('rep-btn');

//=====================================================================================//
//================ Function Untuk Load Quiz Saat Next Button Di Klik  ================//
//===================================================================================//
function nextQuestion() {
  const answer = getValue();
  if (answer) {
    if (initialQuize+1 === Data.length){
      resetButton.style.display = 'block';
    }
    initialQuize++;
    localStorage.setItem('indexQuiz', initialQuize)
    if (initialQuize < Data.length) {
      loadQuize();
    } else if (states.filter(item => item.correct === true).length === Data.length) {
      result.style.display = "flex";
      quiz.style.display = "none";
      repBtn.innerHTML = ` WOW! Ruar Biasa! ${localStorage.getItem('name')} Nilai kamu ${states.filter(item => item.correct === true).length*10}!`;
      audio.pause();
      applause.play();
      applause.loop = true;
    } else if (states.filter(item => item.correct === true).length >= 6){
      result.style.display = "flex";
      quiz.style.display = "none";
      repBtn.innerHTML = ` ${localStorage.getItem('name')} Nilai kamu ${states.filter(item => item.correct === true).length*10}`;
      audio.pause();
      applause.play();
      applause.loop = true;
    } else {
      result.style.display = "flex";
      quiz.style.display = "none";
      repBtn.innerHTML = ` Waduh ${localStorage.getItem('name')} Nilai kamu ${states.filter(item => item.correct === true).length*10} Silahkan Belajar Lagi!`;
      audio.pause();
      dissapointed.play();
    }
    }
}
//=====================================================================================//
//================ Function Untuk Load Quiz Saat Next Button Di Klik  ================//
//===================================================================================//


//=====================================================================================//
//================ Function Untuk Load Quiz Saat Prev Button Di Klik  ================//
//===================================================================================//
function PreviousQuestion() {
  if (initialQuize.valueOf() === 0) {
    alert("Tidak Bisa Kembali Halaman Sebelumnya");
  } else {
    initialQuize--;
    localStorage.setItem('indexQuiz', initialQuize)
    loadQuize();
  }
}
//=====================================================================================//
//================ Function Untuk Load Quiz Saat Prev Button Di Klik  ================//
//===================================================================================//


//=====================================================================================//
//============== Function Untuk ReLoad Quiz Saat Repeat Button Di Klik  ==============//
//===================================================================================//
const resetButton = document.getElementById('Repeat');
resetButton.addEventListener("click", function(){
  formWrap.style.display = 'block';
  questionWrap.style.display = 'none';
  initialQuize = 0
  localStorage.setItem('indexQuiz', 0);
  localStorage.setItem('saveStates', JSON.stringify([]));
  localStorage.removeItem('name');
  states = []
  resetButton.style.display = 'none';
  result.style.display = 'none';
  studentName.value = '';
});
//=====================================================================================//
//============== Function Untuk ReLoad Quiz Saat Repeat Button Di Klik  ==============//
//===================================================================================//


//=====================================================================================//
//================= Function Untuk Change Dark/ Light Mode On Click  =================//
//===================================================================================//
darkModeBtn.addEventListener('click', function(){
  darkMode = !darkMode
  localStorage.getItem('darkMode', darkMode)
  darkMode? document.body.setAttribute('id', 'darkMode'):
  document.body.setAttribute('id', '')
})
//=====================================================================================//
//================= Function Untuk Change Dark/ Light Mode On Click  =================//
//===================================================================================//


//=====================================================================================//
//=========================== Function Untuk Click-Me Footer  ========================//
//===================================================================================//
const floating_btn = document.querySelector('.floating-btn');
const close_btn = document.querySelector('.close-btn');
const social_panel_container = document.querySelector('.social-panel-container');

floating_btn.addEventListener('click', () => {
	social_panel_container.classList.toggle('visible')
});

close_btn.addEventListener('click', () => {
	social_panel_container.classList.remove('visible')
});
//=====================================================================================//
//=========================== Function Untuk Click-Me Footer  ========================//
//===================================================================================//


//=============================================================================================================================//
//=========================================== FUNCTIONS - FUNCTIONS YANG DIGUNAKAN ============================================//
//=============================================================================================================================//

